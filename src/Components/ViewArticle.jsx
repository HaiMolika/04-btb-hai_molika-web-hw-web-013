import React, { Component } from 'react'
import Axios from 'axios';
import {Row, Col} from 'react-bootstrap'

export default class ViewArticle extends Component {
    constructor (props){
        super(props);
        this.state = {
            id : this.props.match.params.id,
            article : {}
        };
    }
    componentWillMount () {
        Axios.get(`http://110.74.194.124:15011/v1/api/articles/${this.state.id}`)
        .then((res) => {
            this.setState({article : res.data.DATA})
        })
    }
    render() {
        return (
            <div className="container">
                <h1>Article</h1>
                <Row>
                    <Col md='4'>
                        <img 
                            style={{width: '150px', height: '270px'}}
                            src={
                                this.state.article.IMAGE === null ?
                                "https://omegamma.com.au/wp-content/uploads/2017/04/default-image.jpg" :
                                this.state.article.IMAGE
                            }
                            alt= 'loading'
                        />
                    </Col>
                    <Col md='8'>
                        <h3>{this.state.article.TITLE}</h3> 
                        <p>{this.state.article.DESCRIPTION}</p> 
                    </Col>
                </Row>
            </div>
        )
    }
}
