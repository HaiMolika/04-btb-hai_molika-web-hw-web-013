import React, { Component } from 'react'
import {Button, Table} from 'react-bootstrap'
import Axios from 'axios';
import {Link} from 'react-router-dom'


export default class ViewTable extends Component {
    constructor () {
        super();
        this.state = {
            article : [],
            image : "https://omegamma.com.au/wp-content/uploads/2017/04/default-image.jpg"
        };
    }
    componentWillMount(){
        console.log("will mount");
        Axios.get("http://110.74.194.124:15011/v1/api/articles?page=1&limit=15")
            .then((res) =>{
                console.log(res.data.DATA)
                this.setState({users : res.data.DATA})
            })
    }
    onDelete = (evt) => {
        Axios.delete(`http://110.74.194.124:15011/v1/api/articles/${evt.target.value}`)
            .then((res) => {
              console.log(res);
              alert(res.data.MESSAGE);
            })
    }

    render() {
        const list = this.state.article.map((data) => {
            let date = data.CREATED_DATE.substring(0,4) + "-" + data.CREATED_DATE.substring(4,6) + "-" + data.CREATED_DATE(6,8);
                                return (
                                    <tr key={data.ID}>
                                        <td  style={{width:'100px'}}>{data.ID}</td>
                                        <td style={{width:'200px'}}>{data.TITLE}</td>
                                        <td style={{width:'250px'}}>{data.DESCRIPTION}</td>
                                        <td style={{width:'150px'}}>{date}</td>
                                        <td style={{width:'150px'}}> 
                                            <img src={data.IMAGE} style={{height : '50px', width: '120px', margin: '5px'}} alt='loading'></img>
                                        </td>
                                        <td style={{width:'250px'}}>
                                            <Link as={Link} to={`/view/${data.ID}`}>
                                                <button className="btn btn-info">view</button>
                                            </Link>
                                            <Link as={Link} to={`/update/${data.ID}`}>
                                                <Button variant="warning" style={{margin : '2px'}}>Edit</Button>
                                            </Link>
                                                <Button type='submit' variant="danger" value={data.ID} style={{margin : '2px'}} onClick={this.onDelete}>Delete</Button>
                                        </td>
                                    </tr>
                                )
                            })
        return (
            <div>
                <center>
                    <h1 style={{margin:'20px'}}>Article Management</h1>
                    <Button variant="dark" as={Link} to='/add'>Add new Article</Button>
                    <div className='container' style={{margin:'20px'}}>
                        <Table striped bordered hover>
                            <thead>
                                <tr>
                                    <th style={{width:'100px'}}>#</th>
                                    <th style={{width:'200px'}}>TITLE</th>
                                    <th style={{width:'250px'}}>DESCRIPTION</th>
                                    <th style={{width:'150px'}}>CREATE DATE</th>
                                    <th style={{width:'150px'}}>IMAGE</th>
                                    <th style={{width:'250'}}>Action</th>
                                </tr>
                            </thead>
                            <tbody>{list}</tbody>
                        </Table>
                    </div>
                </center>
            </div>
        )
    }
}
