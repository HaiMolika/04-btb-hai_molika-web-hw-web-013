import React, { Component } from 'react'
import Axios from 'axios';

export default class Test extends Component {
    constructor(){
        super();
        this.state = {
            users : []
        };
    }
    componentWillMount(){
        console.log("will mount");
        Axios.get("https://reqres.in/api/users?page=2")
            .then((res) =>{
                console.log(res.data.data)
                this.setState({users : res.data.data})
            })

    }
    render() {
        return (
            <div>
                <ul>
                    {this.state.users.map((user) => 
                        <li key={user.id}>{user.first_name}</li>
                    )}
                </ul>
            </div>
        )
    }
}
