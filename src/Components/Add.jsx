import React, { Component } from 'react'
import {Row, Col, Form, Button} from 'react-bootstrap'
import {Link} from 'react-router-dom'
import Axios from 'axios';

export default class Add extends Component {
    constructor() {
        super();
        this.state = {
            title : "", 
            description : "",
            image : "https://omegamma.com.au/wp-content/uploads/2017/04/default-image.jpg"
        };
    }
    titleChange = (e) => {
        this.setState({title : e.target.value})
    }
    descriptionChange = (e) =>{
        this.setState({description : e.target.value})
    }
    addArticle = (evt) =>{
        evt.preventDefault();
        Axios.post("http://110.74.194.124:15011/v1/api/articles",
            {
                TITLE : this.state.title,
                DESCRIPTION : this.state.description,
                IMAGE : this.state.image
            })
            .then((res) => {
                alert(res.data.MESSAGE);
            })
    }
    render() {
        return (
            <div className='container'>
                <Row>
                    <Col md='8'>
                        <h1 style={{margin: '20px'}}>Add Article</h1>
                        <Form>
                            <Form.Group controlId="formBasicEmail">
                                <Form.Label>TITLE</Form.Label>
                                <Form.Control type="text" placeholder="Enter title" onChange={this.titleChange}/>
                            </Form.Group>

                            <Form.Group controlId="formBasicPassword">
                                <Form.Label>Description</Form.Label>
                                <Form.Control type="text" placeholder="Enter description" onChange={this.descriptionChange}/>
                            </Form.Group>
                            <Button variant="primary" type="submit" as={Link} to='/' onClick={this.addArticle}>
                                Submit
                            </Button>
                        </Form>
                    </Col>
                    <Col md='4'> 
                        <img style={{height : '150px', width: '220px', marginTop: '100px'}}src={this.state.image} alt='loading'/>
                    </Col>
                </Row>
            </div>
        )
    }
}
