import React, { Component } from 'react'
import {Row, Col, Form, Button} from 'react-bootstrap'
import Axios from 'axios'
import {Link} from 'react-router-dom'

export default class update extends Component {
    constructor(props){
        super(props);
        this.state = {
            id : this.props.match.params.id,
            image : "https://omegamma.com.au/wp-content/uploads/2017/04/default-image.jpg",
            data : {},
            title : "",
            description : "",
        };
    }
    componentWillMount(){
        Axios.get(`http://110.74.194.124:15011/v1/api/articles/${this.state.id}`)
        .then((res) => {
            this.setState({
                data: res.data.DATA,
            });
            console.log(res.data.DATA);
      })
      this.setState({
        image : this.state.data.IMAGE === null ?
                this.state.image : this.state.data.IMAGE})
    }
    titleChange = (evt) => {
        this.setState ({ title : evt.target.value})
    }
    descriptionChange =(evt) => {
        this.setState({description : evt.target.value})
    }
    updataArticle = (evt) =>{
        evt.preventDefault();
        Axios.put(`http://110.74.194.124:15011/v1/api/articles/${this.state.id}`, 
            {
                TITLE : this.state.title,
                DESCRIPTION : this.state.description,
                IMAGE : this.state.image,
            }).then((res) => {
                    alert(res.data.MESSAGE);
            }
        );
    }
    render() {
        return (
            <div className='container'>
                <Row>
                    <Col md='8'>
                        <h1 style={{margin: '20px'}}>Update Article</h1>
                        <Form>
                            <Form.Group controlId="formBasicEmail">
                                <Form.Label>TITLE</Form.Label>
                                <Form.Control type="text" placeholder="Enter title" onChange={this.titleChange}/>
                            </Form.Group>

                            <Form.Group controlId="formBasicPassword">
                                <Form.Label>Description</Form.Label>
                                <Form.Control type="text" placeholder="Enter description" onChange={this.descriptionChange}/>
                            </Form.Group>
                            <Button variant="primary" type="submit" as={Link} to='/' onClick={this.updataArticle}>
                                Update
                            </Button>
                        </Form>
                    </Col>
                    <Col md='4'> 
                        <img 
                            style={{height : '150px', width: '220px', marginTop: '100px'}}
                            src={this.state.image} 
                            alt='loading'
                        />
                    </Col>
                </Row>
            </div>
        )
    }
}
