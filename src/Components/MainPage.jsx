import React, { Component } from 'react'
import {Navbar, Nav, Form, FormControl, Button} from 'react-bootstrap'
import {Link} from 'react-router-dom'

export default class MainPage extends Component {
    render() {
        return (
            <div>
                <Navbar bg="light" expand="lg">
                    <div className='container'>
                        <Navbar.Brand as={Link} to='/'>Aws</Navbar.Brand>
                        <Navbar.Toggle aria-controls="basic-navbar-nav"/>
                        <Navbar.Collapse id="basic-navbar-nav">
                            <Nav className="mr-auto">
                                <Nav.Link>Home</Nav.Link>
                            </Nav>
                            <Form inline>
                            <FormControl type="text" placeholder="Search" className="mr-sm-2" />
                            <Button variant="outline-success">Search</Button>
                            </Form>
                        </Navbar.Collapse>
                    </div>
                </Navbar>
            </div>
        )
    }
}
