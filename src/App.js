import React from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import MainPage from './Components/MainPage';
import ViewTable from './Components/ViewTable';
import Add from './Components/Add';
import ViewArticle from './Components/ViewArticle';
import update from './Components/update';

function App() {
  return (
    <Router>
      <MainPage/>
      <Switch>
        <Route path='/' exact component={ViewTable}/>
        <Route path='/add' component={Add}/>
        <Route path='/view/:id' component={ViewArticle}/>
        <Route path='/update/:id' component={update}/>
      </Switch>
    </Router>
  );
}

export default App;
